#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:22.04

RUN DEBIAN_FRONTEND=noninteractive apt-get --yes update && \
    DEBIAN_FRONTEND=noninteractive apt-get --yes install \
    python3=3.10.* \
    python3-pip=22.0.* \
    curl=7.81.* \
    unzip \
    && rm -rf /var/lib/apt/lists/*

# Download protobuf zip
ENV PROTOC_URL="https://github.com/protocolbuffers/protobuf/releases/download/v27.2/protoc-27.2-linux-x86_64.zip"
ENV PROTOC_ZIP="protoc-27.2-linux-x86_64.zip"

RUN curl -LO ${PROTOC_URL} && \
    unzip -o ${PROTOC_ZIP} -d ./proto && \
    install -m 0755 ./proto/bin/protoc /usr/local/bin && \
    cp -R ./proto/include/* /usr/local/include/

ENTRYPOINT [ "/usr/local/bin/protoc" ]

